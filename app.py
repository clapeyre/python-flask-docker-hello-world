"""Pipeline tests"""
from flask import Flask

APP = Flask(__name__)

@APP.route("/")
def hello():
    """Return great value."""
    return "Flask inside Docker with AutoDevOps!!"


if __name__ == "__main__":
    APP.run(debug=True, host='0.0.0.0')
